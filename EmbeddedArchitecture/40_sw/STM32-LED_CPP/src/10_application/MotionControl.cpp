/*
 * MotionControl.cpp
 *
 *  Created on: 12.12.2018
 *      Author: Student
 */

#include <MotionControl.h>
#include <cstdlib>
void MotionControl::Update()
{
	uint8_t transmitbuf[4] = "X: ";
	uint8_t transmitValueX[10] = "      mG,";
	uint8_t transmitValueY[10] = "      mG,";
	uint8_t transmitValueZ[10] = "      mG,";
	uint8_t transmitValueTemp[20] = "    degree Celsius,";
	int16_t sensorValue = 0;
	uint16_t newLine = 0x0D0A;

	motionSensor.ReadStatus();

	if(motionSensor.Status.data.ZYX_dataAvailable)
	{
	serialOutputChannel.Write(&transmitbuf[0], 4);
	sensorValue = motionSensor.GetAxisAcceleration(AXIS_X);

	itoa(sensorValue, (char *) &transmitValueX[0], 10);
	serialOutputChannel.Write(&transmitValueX[0], 10);

	transmitbuf[0] = 'Y';
	serialOutputChannel.Write(&transmitbuf[0], 4);
	sensorValue = motionSensor.GetAxisAcceleration(AXIS_Y);

	itoa(sensorValue, (char *) &transmitValueY[0], 10);
	serialOutputChannel.Write(&transmitValueY[0], 10);

	transmitbuf[0] = 'Z';
	serialOutputChannel.Write(&transmitbuf[0], 4);
	sensorValue = motionSensor.GetAxisAcceleration(AXIS_Z);

	itoa(sensorValue, (char *) &transmitValueZ[0], 10);
	serialOutputChannel.Write(&transmitValueZ[0], 10);

	}
	motionSensor.ReadTemperatureStatus();

	if (motionSensor.TemperatureStatus.data.temp_dataAvailable)
	{
		motionSensor.ReadTemperature();
		itoa(motionSensor.TemperatureValue, (char *) &transmitValueTemp[0], 10);
		transmitbuf[0] = 'T';
		serialOutputChannel.Write(&transmitbuf[0], 4);
		serialOutputChannel.Write(&transmitValueTemp[0], 20);
	}
	serialOutputChannel.Write((uint8_t *) &newLine, 2);

}

MotionControl::MotionControl(MotionSensor &motionSensor_, Serial_io &serialOutputChannel_, TimerType &timer_): motionSensor(motionSensor_), serialOutputChannel(serialOutputChannel_), timer(timer_)
{
	serialOutputChannel.Init(19200);
}

void MotionControl::Init()
{
	timer.Init(TIMERBASE_S, 1);
	timer.AddHandler(this);
	timer.Start();
}

MotionControl::~MotionControl() {
	// TODO Auto-generated destructor stub
}

