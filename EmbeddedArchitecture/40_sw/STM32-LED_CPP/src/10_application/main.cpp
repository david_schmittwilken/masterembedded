//
// This file is part of the GNU ARM Eclipse distribution.
// Copyright (c) 2014 Liviu Ionescu.
//

// ----------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include "diag/Trace.h"

#include "stm32f4xx.h"
#include "GPIO.h"
#include "PIN.h"
#include "TIM2_IRQHandler.h"
#include "LedControl.h"
#include "PingPongUart.h"
#include "I2C1EVIRQHandler.h"
#include "MotionSensor.h"
#include "USART1_IRQHandler.h"
#include "MotionControl.h"
// ----- main() ---------------------------------------------------------------

// Sample pragmas to cope with warnings. Please note the related line at
// the end of this function, used to pop the compiler diagnostics status.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wmissing-declarations"
#pragma GCC diagnostic ignored "-Wreturn-type"

int main(int argc, char* argv[]) {

	CTIM2_IRQHandler motionControlTimer = CTIM2_IRQHandler();
	I2C1EVIRQHandler motionSensorHwInterface= I2C1EVIRQHandler();

	MotionSensor mySensor = MotionSensor(motionSensorHwInterface, 0x18);
	USART1_IRQHandler senseEvaluate = USART1_IRQHandler();
	MotionControl motionControl = MotionControl(mySensor, senseEvaluate, motionControlTimer);
	motionControl.Init();

//	GPIO gpio_led = GPIO(*GPIOG);
//	GPIO gpio_button = GPIO(*GPIOA);
//	PIN led = PIN(gpio_led, 13);
//	PIN referenceLed = PIN(gpio_led, 14);
//	PIN button = PIN(gpio_button, 0);
//	gpio_led.InitPin(13, OUTPUT_PUSHPULL_PULLUP);
//	gpio_led.InitPin(14, OUTPUT_PUSHPULL_PULLUP);
//	gpio_button.InitPin(0, INPUT_FLOATING);
//
//
//	referenceLed.Set(1);

	//LedControl ledControl = LedControl(timer, led, button);

	while (1) {
asm("NOP");
		//ledControl.EvaluateLedControl();
		//myPingPongUart.EvaluateData();
	}
}

#pragma GCC diagnostic pop

// ----------------------------------------------------------------------------
