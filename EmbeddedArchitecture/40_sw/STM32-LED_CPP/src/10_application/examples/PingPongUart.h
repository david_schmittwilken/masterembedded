/*
 * PingPongUart.h
 *
 *  Created on: 02.12.2018
 *      Author: dave
 */

#ifndef PINGPONGUART_H_
#define PINGPONGUART_H_

class PingPongUart
{
public:
	void EvaluateData();
	PingPongUart();
	virtual ~PingPongUart();
};

#endif /* 10_APPLICATION_PINGPONGUART_H_ */
