/*
 * PingPongUart.cpp
 *
 *  Created on: 02.12.2018
 *      Author: dave
 */

#include <PingPongUart.h>
#include "USART1_IRQHandler.h"

USART1_IRQHandler uartTransmit = USART1_IRQHandler();
uint8_t data1[64] = "Hallo Bot1! ";
uint8_t data2[64] = "Hallo Bot2! ";


PingPongUart::PingPongUart()
{
	// TODO Auto-generated constructor stub
	uartTransmit.Init(UART_BAUD_19200);
}

void PingPongUart::EvaluateData()
{
	static uint8_t currentCommand = 0;
	uint8_t size_re = 0;
	uint8_t readBuf[64];
	uartTransmit.Read(&readBuf[0], 64, &size_re);
	if(size_re > 0)
	{
		if(size_re == 1)
		{
			if((readBuf[0] >= '0') && (readBuf[0] <= '9'))
			{
			currentCommand = readBuf[0];
			}
		}
		switch(currentCommand)
		{
		case '0': uartTransmit.Write(&readBuf[0], size_re);
				break;

		case '1': uartTransmit.Write(&data1[0], 64);
				break;
		case '2': uartTransmit.Write(&data2[0], 64);
				break;
		case '3':
				break;
		}

	}

}

PingPongUart::~PingPongUart()
{
	// TODO Auto-generated destructor stub
}

