/*
 * LedControl.cpp
 *
 *  Created on: 23.10.2018
 *      Author: dave
 */

#include "LedControl.h"

#include "stm32f4xx.h"
#include "GPIO.h"
#include "PIN.h"
#include "BasicTimerType.h"
#include "USART1_IRQHandler.h"


void LedControl::InitLedControl(void)
{
	//nothing to do
}

LedControl::LedControl(TimerType &timer_, PIN &led_, PIN &button_):timer(timer_), led(led_), button(button_)
{
	timer.AddHandler(this);
	timer.Start();
}

void LedControl::EvaluateLedControl(void)
{
	timer.Pause();
	if(led.Get() == 0)
	{
	led.Set(button.Get());
	}
	timer.Start();
}

void LedControl::Update()
{
	led.Toggle();
}



