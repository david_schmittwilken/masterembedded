/*
 * LedControl.h
 *
 *  Created on: 23.10.2018
 *      Author: dave
 */

#ifndef LEDCONTROL_H_
#define LEDCONTROL_H_

#include "TimerType.h"
#include "PIN.h"
class LedControl : public Handler
{
public:
	TimerType &timer;
	PIN &led;
	PIN &button;
	void InitLedControl(void);
	void EvaluateLedControl(void);
	void Update(void);

	LedControl(TimerType &timer_, PIN &led_, PIN &button_);
	~LedControl(){};
};




#endif /* 10_APPLICATION_LEDCONTROL_H_ */
