/*
 * MotionControl.h
 *
 *  Created on: 12.12.2018
 *      Author: Student
 */

#ifndef MOTIONCONTROL_H_
#define MOTIONCONTROL_H_
#include "MotionSensor.h"
#include "Handler.h"
#include "Serial_io.h"
#include "TimerType.h"



class MotionControl: public Handler {
public:
	MotionSensor &motionSensor;
	Serial_io &serialOutputChannel;
	TimerType &timer;

	void Update(void);
	void Init(void);
	MotionControl(MotionSensor &motionSensor_, Serial_io &serialOutputChannel_, TimerType &timer_);
	virtual ~MotionControl();
};

#endif /* 10_APPLICATION_MOTIONCONTROL_H_ */
