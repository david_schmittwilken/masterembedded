/*
 * TIM2_IRQHandler.h
 *
 *  Created on: 11.11.2018
 *      Author: dave
 */


#ifndef TIM2_IRQHANDLER_H_
#define TIM2_IRQHANDLER_H_

#include "Handler.h"
#include "stm32f4xx.h"
#include "BasicTimerType.h"


class CTIM2_IRQHandler : public BasicTimerType
{
public:
	static CTIM2_IRQHandler *myThis;
	CTIM2_IRQHandler();
private:


};

#endif /* 20_DRIVER_TIM1_CC_IRQHANDLER_H_ */
