/*
 * BasicTimerType.cpp
 *
 *  Created on: 02.11.2018
 *      Author: dave
 */
#include "BasicTimerType.h"
#include "stm32f4xx.h"
#include "macros.h"
#include <cstdlib>
#include <limits>
#include <cmath>
#include "Handler.h"
#include "FermatFactorization.h"

#include "TIM1_CC_IRQHandler.h"
#include "TIM2_IRQHandler.h"
#include "TIM3_IRQHandler.h"

#define APB1_KHZ_MAX 42000
#define APB2_KHZ_MAX 84000

#define APB1_TIMER_CLK_SRC_FREQ_MAX_KHZ APB1_KHZ_MAX * 2
#define APB2_TIMER_CLK_SRC_FREQ_MAX_KHZ APB2_KHZ_MAX * 2

void BasicTimerType::Init(TIMERBASE timerBase_, uint16_t factor_)
{
	switch ((uint32_t) &timTypeDef)
	{
	case (uint32_t) TIM1_BASE:
		RCC->APB2ENR |= RCC_APB2ENR_TIM1EN;
		this->interruptIndex = TIM1_CC_IRQn;
		this->timerClockSrcFrequency_KHz = APB2_TIMER_CLK_SRC_FREQ_MAX_KHZ;
		this->prescalerSize = PRESCALERSIZE_16BIT;
		this->prescalerMaxValue = UINT32_MAX;
		break;
	case (uint32_t) TIM2_BASE:
		RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;
		this->interruptIndex = TIM2_IRQn;
		this->timerClockSrcFrequency_KHz = APB1_TIMER_CLK_SRC_FREQ_MAX_KHZ;
		this->prescalerSize = PRESCALERSIZE_32BIT;
		this->prescalerMaxValue = (uint64_t) ((uint64_t) UINT16_MAX
				* (uint64_t) UINT32_MAX);
		break;
	case (uint32_t) TIM3_BASE:
		RCC->APB1ENR |= RCC_APB1ENR_TIM3EN;
		this->interruptIndex = TIM3_IRQn;
		this->timerClockSrcFrequency_KHz = APB1_TIMER_CLK_SRC_FREQ_MAX_KHZ;
		this->prescalerSize = PRESCALERSIZE_16BIT;
		this->prescalerMaxValue = UINT32_MAX;
		break;
	case (uint32_t) TIM4_BASE:
		RCC->APB1ENR |= RCC_APB1ENR_TIM4EN;
		this->interruptIndex = TIM4_IRQn;
		this->timerClockSrcFrequency_KHz = APB1_TIMER_CLK_SRC_FREQ_MAX_KHZ;
		this->prescalerSize = PRESCALERSIZE_16BIT;
		this->prescalerMaxValue = UINT32_MAX;
		break;
	case (uint32_t) TIM5_BASE:
		RCC->APB1ENR |= RCC_APB1ENR_TIM5EN;
		this->interruptIndex = TIM5_IRQn;
		this->timerClockSrcFrequency_KHz = APB1_TIMER_CLK_SRC_FREQ_MAX_KHZ;
		this->prescalerSize = PRESCALERSIZE_32BIT;
		this->prescalerMaxValue = (uint64_t) ((uint64_t) UINT16_MAX
				* (uint64_t) UINT32_MAX);
		break;
	case (uint32_t) TIM6_BASE:
		RCC->APB1ENR |= RCC_APB1ENR_TIM6EN;
		this->interruptIndex = TIM6_DAC_IRQn;
		this->timerClockSrcFrequency_KHz = APB1_TIMER_CLK_SRC_FREQ_MAX_KHZ;
		this->prescalerSize = PRESCALERSIZE_16BIT;
		this->prescalerMaxValue = UINT32_MAX;
		break;
	case ((uint32_t) TIM7_BASE):
		RCC->APB1ENR |= RCC_APB1ENR_TIM7EN;
		this->interruptIndex = TIM7_IRQn;
		this->timerClockSrcFrequency_KHz = APB1_TIMER_CLK_SRC_FREQ_MAX_KHZ;
		this->prescalerSize = PRESCALERSIZE_16BIT;
		this->prescalerMaxValue = UINT32_MAX;
		break;
	case ((uint32_t) TIM8_BASE):
		RCC->APB2ENR |= RCC_APB2ENR_TIM8EN;
		this->interruptIndex = TIM8_CC_IRQn;
		this->timerClockSrcFrequency_KHz = APB2_TIMER_CLK_SRC_FREQ_MAX_KHZ;
		this->prescalerSize = PRESCALERSIZE_16BIT;
		this->prescalerMaxValue = UINT32_MAX;
		break;
	case ((uint32_t) TIM9_BASE):
		RCC->APB2ENR |= RCC_APB2ENR_TIM9EN;
		this->interruptIndex = TIM1_BRK_TIM9_IRQn;
		this->timerClockSrcFrequency_KHz = APB2_TIMER_CLK_SRC_FREQ_MAX_KHZ;
		this->prescalerSize = PRESCALERSIZE_16BIT;
		this->prescalerMaxValue = UINT32_MAX;
		break;
	case ((uint32_t) TIM10_BASE):
		RCC->APB2ENR |= RCC_APB2ENR_TIM10EN;
		this->interruptIndex = TIM1_UP_TIM10_IRQn;
		this->timerClockSrcFrequency_KHz = APB2_TIMER_CLK_SRC_FREQ_MAX_KHZ;
		this->prescalerSize = PRESCALERSIZE_16BIT;
		this->prescalerMaxValue = UINT32_MAX;
		break;
	case ((uint32_t) TIM11_BASE):
		RCC->APB2ENR |= RCC_APB2ENR_TIM11EN;
		this->interruptIndex = TIM1_TRG_COM_TIM11_IRQn;
		this->timerClockSrcFrequency_KHz = APB2_TIMER_CLK_SRC_FREQ_MAX_KHZ;
		this->prescalerSize = PRESCALERSIZE_16BIT;
		this->prescalerMaxValue = UINT32_MAX;
		break;
	case ((uint32_t) TIM12_BASE):
		RCC->APB1ENR |= RCC_APB1ENR_TIM12EN;
		this->interruptIndex = TIM8_BRK_TIM12_IRQn;
		this->timerClockSrcFrequency_KHz = APB1_TIMER_CLK_SRC_FREQ_MAX_KHZ;
		this->prescalerSize = PRESCALERSIZE_16BIT;
		this->prescalerMaxValue = UINT32_MAX;
		break;
	case ((uint32_t) TIM13_BASE):
		RCC->APB1ENR |= RCC_APB1ENR_TIM13EN;
		this->interruptIndex = TIM8_UP_TIM13_IRQn;
		this->timerClockSrcFrequency_KHz = APB1_TIMER_CLK_SRC_FREQ_MAX_KHZ;
		this->prescalerSize = PRESCALERSIZE_16BIT;
		this->prescalerMaxValue = UINT32_MAX;
		break;
	case ((uint32_t) TIM14_BASE):
		RCC->APB1ENR |= RCC_APB1ENR_TIM7EN;
		this->interruptIndex = TIM8_TRG_COM_TIM14_IRQn;
		this->timerClockSrcFrequency_KHz = APB1_TIMER_CLK_SRC_FREQ_MAX_KHZ;
		this->prescalerSize = PRESCALERSIZE_16BIT;
		this->prescalerMaxValue = UINT32_MAX;
		break;
	default:
		break;
	}

	/* Clear the Update event flag */
	timTypeDef.SR = 0;
	/*prescaler*/
	setPeriodTime(timerBase_, factor_);

	RESETBITS(timTypeDef.CR1, UINT32_MAX, 0);

	SETBITS(timTypeDef.CR1, ARPE_BUFFERED, CR1_BITBASE_ARPE);
	SETBITS(timTypeDef.CR1, CMS_EDGEALIGNED, CR1_BITBASE_CMS);
	SETBITS(timTypeDef.CR1, DIR_UPCOUNTER, CR1_BITBASE_DIR);
	SETBITS(timTypeDef.CR1, ONEPULSE_OFF, CR1_BITBASE_OPM);
	SETBITS(timTypeDef.CR1, URS_OVER_UNDER, CR1_BITBASE_URS);
	SETBITS(timTypeDef.CR1, UDIS_UEV_ENABLE, CR1_BITBASE_UDIS);
	SETBITS(timTypeDef.CR1, CEN_ENABLE, CR1_BITBASE_CEN);

	RESETBITS(timTypeDef.CR2, UINT32_MAX, 0);

	SETBITS(timTypeDef.CR2, MMS_RESET, CR2_BITBASE_MMS);

	RESETBITS(timTypeDef.DIER, UINT32_MAX, 0);

	/*Update interrupt en*/
	SETBITS(timTypeDef.DIER, 1, 0);

}

void BasicTimerType::setPeriodTime(TIMERBASE timerBase_, uint16_t factor_)
{
	bool initValid = true;
	uint64_t prescaler = 0;
	uint32_t prescaler1 = 0;
	uint16_t prescaler2 = 0;
	bool prescalerValid = false;

	switch (timerBase_)
	{
	case TIMERBASE_US:
		if (((uint64_t) (uint64_t) factor_
				* ((uint64_t) this->timerClockSrcFrequency_KHz / (uint64_t) 1000))
				< this->prescalerMaxValue)
		{
			prescaler = (uint64_t) ((uint64_t) factor_
					* ((uint64_t) this->timerClockSrcFrequency_KHz
							/ (uint64_t) 1000));
			prescalerValid = true;
		}
		break;
	case TIMERBASE_MS:
		if (((uint64_t) (uint64_t) factor_
				* ((uint64_t) this->timerClockSrcFrequency_KHz))
				< (uint64_t) this->prescalerMaxValue)
		{
			prescaler = (uint64_t) ((uint64_t) factor_
					* ((uint64_t) this->timerClockSrcFrequency_KHz));
			prescalerValid = true;
		}
		break;
	case TIMERBASE_S:
		if (((uint64_t) (uint64_t) factor_
				* ((uint64_t) this->timerClockSrcFrequency_KHz * 1000))
				< this->prescalerMaxValue)
		{
			prescaler = (uint64_t) ((uint64_t) factor_
					* ((uint64_t) this->timerClockSrcFrequency_KHz
							* (uint64_t) 1000));
			prescalerValid = true;
		}
		break;

	default:
		break;
	}

	if (prescalerValid == true)
	{
		if (this->prescalerSize == PRESCALERSIZE_32BIT)
		{
			initValid = findPrescaler(prescaler, prescaler1, prescaler2);
		}
		else if (this->prescalerSize == PRESCALERSIZE_16BIT)
		{

			if (prescaler > UINT16_MAX)
			{
				FermatFactorization::FermatFactor((uint32_t)prescaler, prescaler1, prescaler2);
			}
			else
			{
				prescaler1 = prescaler;
				prescaler2 = 0;
			}
			initValid = true;
		}
		if (initValid == true)
		{
			RESETBITS(timTypeDef.ARR, UINT32_MAX, 0);
			SETBITS(timTypeDef.ARR, prescaler1, 0);

			RESETBITS(timTypeDef.PSC, UINT16_MAX, 0);
			SETBITS(timTypeDef.PSC, prescaler2, 0);

		}
	}
}

/*TODO: how to divide a number by 2 factors with values in relation 2 to 1?*/
bool BasicTimerType::findPrescaler(uint64_t &prescaler, uint32_t &prescaler1,
		uint16_t &prescaler2)
{
	bool prescalerValid = true;

	if (prescaler > 0)
	{
		if (prescaler <= UINT32_MAX)
		{
			prescaler1 = (uint32_t) prescaler;
			prescaler2 = 0;
		}
		else
		{
			while (prescalerValid && prescaler > UINT32_MAX
					&& prescaler2 < UINT16_MAX)
			{
				uint16_t divider = 2;
				while (prescaler % divider != 0 && divider < UINT16_MAX)
				{
					divider++;
				}

				if (prescaler % divider == 0)
				{

					if ((prescaler2 > 0)
							&& ((prescaler2 * divider) < UINT16_MAX))
					{
						prescaler /= divider;
						prescaler2 *= divider;
					}
					else if ((prescaler2 > 0)
							&& ((prescaler2 * 2) < UINT16_MAX))
					{
						/*round because divider exceeds 16 bit limit*/
						prescaler /= 2;
						prescaler2 *= 2;
					}
					else if ((prescaler2 > 0)
							&& ((prescaler2 * 2)
									< UINT16_MAX + ((prescaler2 * 2) / 10)))
					{
						/*round because divider exceeds 16 bit limit*/
						prescaler = (prescaler / 2) + (prescaler2 / 20);
						prescaler2 = UINT16_MAX;
					}
					else if (prescaler2 == 0)
					{
						prescaler /= divider;
						prescaler2 = divider;
					}
					else
					{
						prescalerValid = false;
					}
				}
				else
				{
					prescalerValid = false;
				}

			}
			if (prescalerValid == true)
			{
				prescaler1 = prescaler;
				prescaler2--;
			}
		}
	}
	return prescalerValid;
}

void BasicTimerType::Start()
{
	/* Clear the Update event flag */
	NVIC_EnableIRQ(interruptIndex);
}

void BasicTimerType::Pause()
{
	/* Clear the Update event flag */
	NVIC_DisableIRQ(interruptIndex);
}

void BasicTimerType::ResetStatus()
{
	/* Clear the Update event flag */
	timTypeDef.SR = 0;
}



void BasicTimerType::Update() {
	ResetStatus();
	for (int i = 0; i < HANDLERLIST_COUNT; i++) {
		if (handler[i] != NULL) {
			handler[i]->Update();
		}
	}
}


BasicTimerType::BasicTimerType(TIM_TypeDef &timTypeDef_) : TimerType(timTypeDef_)
{
		currentHandlerIndex = 0;
}

