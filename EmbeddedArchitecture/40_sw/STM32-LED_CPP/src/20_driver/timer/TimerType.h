/*
 * TimerType.h
 *
 *  Created on: 02.11.2018
 *      Author: dave
 */

#ifndef TIMERTYPE_H_
#define TIMERTYPE_H_
#include <stdio.h>
#include "TimerType.h"
#include "stm32f4xx.h"

#include "Observable.h"

enum TIMERBASE
{
TIMERBASE_US = 0,
TIMERBASE_MS = 1,
TIMERBASE_S = 2,
};

class TimerType : public Observable
{
public:
	uint64_t counter = 0;
	TIM_TypeDef &timTypeDef;

	virtual void Init(TIMERBASE timerBase_, uint16_t factor_) = 0;
	virtual void Start() = 0;
	virtual void Pause() = 0;
	virtual void ResetStatus() = 0;


	TimerType(TIM_TypeDef &timTypeDef_) : Observable(), timTypeDef(timTypeDef_)
	{

	};
	~TimerType(){};

protected:

	uint32_t timerClockSrcFrequency_KHz = 0;
};



#endif /* TIMERTYPE_H_ */
