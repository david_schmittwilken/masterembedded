/*
 * BasicTimerType.h
 *
 *  Created on: 02.11.2018
 *      Author: dave
 */

#ifndef BASICTIMERTYPE_H_
#define BASICTIMERTYPE_H_
#include "TimerType.h"
#include "stm32f4xx.h"
#include "Handler.h"
/*typedefs*/
/*CR1 Register*/
enum CR1_BITBASE
{
CR1_BITBASE_CEN = 0,
CR1_BITBASE_UDIS = 1,
CR1_BITBASE_URS = 2,
CR1_BITBASE_OPM = 3,
CR1_BITBASE_DIR = 4,
CR1_BITBASE_CMS = 5,
CR1_BITBASE_ARPE = 7,
};

enum ARPE
{
	ARPE_UNBUFFERED = 0,
	ARPE_BUFFERED = 1,
};

enum CMS
{
	CMS_EDGEALIGNED = 0,
	CMS_CENTERALIGNED1 = 1,
	CMS_CENTERALIGNED2 = 2,
	CMS_CENTERALIGNED3 = 3,
};

enum DIR
{
	DIR_UPCOUNTER = 0,
	DIR_DOWNCOUNTER = 1,
};

/*ONEPULSE_OFF: Counter is not stopped at update event, ONEPULSE_ON: Counter stops counting at the next update event (clearing the CEN bit).*/
enum ONEPULSE
{
	ONEPULSE_OFF = 0,
	ONEPULSE_ON = 1,
};

enum URS
{
	URS_ANY = 0,
	URS_OVER_UNDER = 1,
};

enum UDIS
{
UDIS_UEV_ENABLE = 0,
UDIS_UEV_DISABLE = 1,
};

enum CEN
{
	CEN_DISABLE = 0,
	CEN_ENABLE = 1,
};
/*~CR1 Register*/

/*CR2 Register*/
enum CR2_BITBASE
{
CR2_BITBASE_MMS = 4,
};

enum MMS
{
	MMS_RESET = 0,
	MMS_ENABLE = 1,
	MMS_UPDATE = 2,
};


/*~CR2 Register*/

enum PRESCALERSIZE
{
	PRESCALERSIZE_16BIT = 0,
	PRESCALERSIZE_32BIT = 1,
};


class BasicTimerType : public TimerType
{
public:
	void Init(TIMERBASE timerBase_, uint16_t factor_);
	void Start();
	void Pause();
	void ResetStatus();
	void Update();
	BasicTimerType(TIM_TypeDef &timTypeDef_);
	~BasicTimerType(){};
protected:

private:
	enum PRESCALERSIZE prescalerSize;
	uint64_t prescalerMaxValue;

	void setPeriodTime(TIMERBASE timerBase_, uint16_t factor_);
	static bool findPrescaler(uint64_t &prescaler, uint32_t &prescaler1, uint16_t &prescaler2);
};



#endif /* 20_DRIVER_BASICTIMERTYPE_H_ */
