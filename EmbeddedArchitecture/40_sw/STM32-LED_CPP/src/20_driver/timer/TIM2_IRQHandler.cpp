/*
 * TIM2_IRQHandler.cpp
 *
 *  Created on: 11.11.2018
 *      Author: dave
 */

#include "TIM2_IRQHandler.h"
#include <cstdlib>
#include "stm32f4xx.h"
#include "Handler.h"

CTIM2_IRQHandler *CTIM2_IRQHandler::myThis= NULL;

CTIM2_IRQHandler::CTIM2_IRQHandler():BasicTimerType(*TIM2)
{
	CTIM2_IRQHandler::myThis = this;
}

extern "C"
{
void TIM2_IRQHandler(void)
{
	if (CTIM2_IRQHandler::myThis != NULL)
	{
		CTIM2_IRQHandler::myThis->Update();
	}
}
}

