/*
 * Serial_io.h
 *
 *  Created on: 26.11.2018
 *      Author: dave
 */

#ifndef SERIAL_IO_H_
#define SERIAL_IO_H_

#include <cstdint>
#include <cstdlib>
#include <cstdio>
#include "stm32f4xx.h"
#include "CircularBuffer.h"
#include "Observable.h"

class Serial_io : public Observable
{
public:
	static Serial_io *myThis;
	CircularBuffer<std::uint8_t, 128U> TxBuffer;
	CircularBuffer<std::uint8_t, 128U> RxBuffer;
	virtual uint8_t Init(uint32_t baudrate_) = 0;
	virtual uint8_t Write(uint8_t *data, uint8_t size) = 0;
	virtual uint8_t Read(uint8_t *data_, uint8_t buffer_size_, uint8_t *size_re_) = 0;

	Serial_io() : Observable()
	{
	}
	;
	~Serial_io(){};

};

#endif /* 20_DRIVER_IO_SERIAL_IO_H_ */
