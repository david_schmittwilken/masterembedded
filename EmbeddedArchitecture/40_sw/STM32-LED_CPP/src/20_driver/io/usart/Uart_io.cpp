/*
 * Uart_io.cpp
 *
 *  Created on: 26.11.2018
 *      Author: dave
 */

#include "GPIO.h"
#include "Uart_io.h"

Uart_io::Uart_io(USART_TypeDef &usart_):usart(usart_)
{

}

/*TODO: add uartconfiguration enum for some default configurations*/
uint8_t Uart_io::Init(uint32_t baudrate_)
{
	uint8_t result = 0;

	// Enable USART clock
	switch ((uint32_t) &usart) {
	case (uint32_t) USART1_BASE:
		RCC->APB2ENR |= RCC_APB2ENR_USART1EN;
	this->interruptIndex = USART1_IRQn;
		break;
	case ((uint32_t) USART2_BASE):
		RCC->AHB1ENR |= RCC_APB1ENR_USART2EN;
	this->interruptIndex = USART2_IRQn;
		break;
	case ((uint32_t) USART3_BASE):
		RCC->AHB1ENR |= RCC_APB1ENR_USART3EN;
	this->interruptIndex = USART3_IRQn;
		break;
	case ((uint32_t) UART4_BASE):
		RCC->AHB1ENR |= RCC_APB1ENR_UART4EN;
	this->interruptIndex = UART4_IRQn;
		break;
	case ((uint32_t) UART5_BASE):
		RCC->AHB1ENR |= RCC_APB1ENR_UART5EN;
	this->interruptIndex = UART5_IRQn;
		break;
	case ((uint32_t) USART6_BASE):
		RCC->AHB2ENR |= RCC_APB2ENR_USART6EN;
	this->interruptIndex = USART6_IRQn;
		break;
	default:
		break;
	}

	usart.BRR = UART_CLK_SRC / baudrate_;
	// | USART_CR1_TXEIE
	usart.CR1 = USART_CR1_UE | USART_CR1_RXNEIE | USART_CR1_TE | USART_CR1_RE;
	NVIC_EnableIRQ(interruptIndex);
	return result;
}
uint8_t Uart_io::Write(uint8_t *data, uint8_t size)
{
	uint8_t result = 0;

	for(uint32_t i = 0; i< size; i++)
	{
		TxBuffer.in(data[i]);
	}

	usart.CR1 |= USART_CR1_TCIE;
	return result;
}
uint8_t Uart_io::Read(uint8_t *data_, uint8_t buffer_size_, uint8_t *size_re_)
{
	uint8_t result = 0;

	if (!RxBuffer.empty())
	{
		for (uint32_t i = 0; i < buffer_size_; i++)
		{
			if (!RxBuffer.empty())
			{
				data_[i] = RxBuffer.out();
			}
			else
			{
				*size_re_ = i;
				break;
			}
		}
		result = 1;
	}
	return result;
}



void Uart_io::Update() {

	if (usart.SR & USART_FLAG_RXNE)
	{
		RxBuffer.in(usart.DR);
		usart.CR1 |= USART_CR1_TCIE;
	}
	if ((!TxBuffer.empty()))
	{
		if((usart.SR & USART_SR_TXE))
		{
		usart.DR = TxBuffer.out();
		}
	}
	else
	{
		usart.CR1 &= ~USART_CR1_TCIE;
	}
}

