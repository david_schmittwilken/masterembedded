/*
 * Uart_io.h
 *
 *  Created on: 26.11.2018
 *      Author: dave
 */

#ifndef UART_IO_H_
#define UART_IO_H_

#include "stm32f4xx.h"
#include <cstdlib>
#include "Serial_io.h"

/*constants*/
#define UART_CLK_SRC (84E6)
#define UART_BAUD_19200 (19200)
/*typedefs*/

/*CR1 Register*/
enum UART_CONFIG_CR2
{	CR2_STOPPBITS_1 = 0,
	CR2_STOPPBITS_0_5 = USART_CR2_STOP_0,
	CR2_STOPPBITS_2 = USART_CR2_STOP_1,
	CR2_STOPPBITS_1_5 = USART_CR2_STOP_0 | USART_CR2_STOP_1,
};

enum UARTCONFIGURATION
{
	DATA8_NOPARITY_STOP1_NO_FLOW = 0,
};

class Uart_io: public Serial_io
{
public:
	USART_TypeDef &usart;

	uint8_t Init(uint32_t baudrate_);
	uint8_t Write(uint8_t *data, uint8_t size);
	uint8_t Read(uint8_t *data_, uint8_t buffer_size_, uint8_t *size_re_);
	Uart_io(USART_TypeDef &usart_);
	~Uart_io()
	{
	}
	;
	void Update();

};

#endif /* 20_DRIVER_IO_UART_IO_H_ */
