/*
 * USART1_IRQHandler.h
 *
 *  Created on: 02.12.2018
 *      Author: dave
 */

#ifndef USART1_IRQHANDLER_H_
#define USART1_IRQHANDLER_H_

#include "Handler.h"
#include "stm32f4xx.h"
#include "Uart_io.h"


class USART1_IRQHandler : public Uart_io
{
public:
	static USART1_IRQHandler *myThis;
	USART1_IRQHandler();
private:


};



#endif /* 20_DRIVER_IO_USART1_IRQHANDLER_H_ */
