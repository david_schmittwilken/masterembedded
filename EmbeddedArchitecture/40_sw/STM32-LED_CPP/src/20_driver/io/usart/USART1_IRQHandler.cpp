/*
 * USART1_IRQHandler.cpp
 *
 *  Created on: 02.12.2018
 *      Author: dave
 */

#include "USART1_IRQHandler.h"
#include <cstdlib>
#include "stm32f4xx.h"
#include "Handler.h"
#include "GPIO.h"

USART1_IRQHandler *USART1_IRQHandler::myThis= NULL;

USART1_IRQHandler::USART1_IRQHandler():Uart_io(*USART1)
{
	GPIO gpio_uart = GPIO(*GPIOA);
	gpio_uart.InitPin(9, OUTPUT_ALTERNATEFUNCTION, ALTERNATEFUNCTION7);
	gpio_uart.InitPin(10, INPUT_ALTERNATEFUNCTION, ALTERNATEFUNCTION7);
	USART1_IRQHandler::myThis = this;
}

extern "C"
{
void USART1_IRQHandler(void)
{
	if (USART1_IRQHandler::myThis != NULL)
	{
		USART1_IRQHandler::myThis->Update();
	}
}
}



