/*
 * I2cio.h
 *
 *  Created on: 03.12.2018
 *      Author: dave
 */

#ifndef I2CIO_H_
#define I2CIO_H_

#include "stm32f4xx.h"
#include <cstdlib>
#include "Serial_io.h"

/*constants*/
#define I2C_CLK_SRC (42)
#define I2C_BAUD_100KHz (100000)
#define I2C_BAUD_400KHz (400000)
/*typedefs*/

/*CR1 Register*/
enum I2C_CONFIG_CR2
{
	I2C_CONFIG_CR2_TEST1 = 0,
};

enum I2C_CONFIGURATION
{
	I2C_CONFIG1 = 0,
};

typedef enum
{
  I2C_STATE_RESET,   			/*!< Peripheral is not yet Initialized         */
  I2C_STATE_READY,   			/*!< Peripheral Initialized and ready for use  */
  I2C_STATE_BUSY,   			/*!< An internal process is ongoing            */
  I2C_STATE_BUSY_TX,   			/*!< Data Transmission process is ongoing      */
  I2C_STATE_BUSY_RX,   			/*!< Data Reception process is ongoing         */
  I2C_STATE_LISTEN,   			/*!< Address Listen Mode is ongoing            */
  I2C_STATE_BUSY_TX_LISTEN,   	/*!< Address Listen Mode and Data Transmission
                                                 process is ongoing                         */
  I2C_STATE_BUSY_RX_LISTEN,   	/*!< Address Listen Mode and Data Reception
                                                 process is ongoing                         */
  I2C_STATE_TIMEOUT,   			/*!< Timeout state                             */
  I2C_STATE_ERROR   			/*!< Error                                     */

}I2C_States;

class I2c_io: public Serial_io
{
public:
	I2C_TypeDef &i2c;
	I2C_States I2cState = I2C_STATE_RESET;
	uint8_t CurrentRegister = 0;
	uint8_t CurrentAddress = 0x18;
	uint8_t Init(uint32_t baudrate_);
	uint8_t Write(uint8_t *data, uint8_t size);
	uint8_t Read(uint8_t *data_, uint8_t buffer_size_, uint8_t *size_re_);

	I2c_io(I2C_TypeDef &i2c_);
	~I2c_io(){};
	void Update();
};

#endif /* 20_DRIVER_IO_I2CIO_H_ */
