/*
 * I2C1EVIRQHandler.cpp
 *
 *  Created on: 06.12.2018
 *      Author: dave
 */

#include <I2C1EVIRQHandler.h>

#include <cstdlib>
#include "stm32f4xx.h"
#include "Handler.h"
#include "GPIO.h"

I2C1EVIRQHandler *I2C1EVIRQHandler::myThis= NULL;
//8: SCL 9:SDA
I2C1EVIRQHandler::I2C1EVIRQHandler():I2c_io(*I2C1)
{
	GPIO gpio_i2c = GPIO(*GPIOB);
	gpio_i2c.InitPin(8, OUTPUT_ALTERNATEFUNCTION, ALTERNATEFUNCTION4);
	gpio_i2c.InitPin(9, OUTPUT_ALTERNATEFUNCTION, ALTERNATEFUNCTION4);
	I2C1EVIRQHandler::myThis = this;
}

extern "C"
{
void I2C1EVIRQHandler(void)
{
	if (I2C1EVIRQHandler::myThis != NULL)
	{
		I2C1EVIRQHandler::myThis->Update();
	}
}
}
