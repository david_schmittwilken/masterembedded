/*
 * I2cio.cpp
 *
 *  Created on: 03.12.2018
 *      Author: dave
 */

#include <I2c_io.h>

I2c_io::I2c_io(I2C_TypeDef &i2c_):i2c(i2c_)
{
	// TODO Auto-generated constructor stub

}


/*TODO: add uartconfiguration enum for some default configurations*/
uint8_t I2c_io::Init(uint32_t baudrate_)
{
	uint8_t result = 0;

	// Enable i2c clock
	switch ((uint32_t) &i2c) {
	case (uint32_t) I2C1_BASE:
		RCC->APB1ENR |= RCC_APB1ENR_I2C1EN;
	this->interruptIndex = I2C1_EV_IRQn;
		break;
	case ((uint32_t) I2C2_BASE):
		RCC->AHB1ENR |= RCC_APB1ENR_I2C2EN;
	this->interruptIndex = I2C2_EV_IRQn;
		break;
	case ((uint32_t) I2C3_BASE):
		RCC->AHB1ENR |= RCC_APB1ENR_I2C3EN;
	this->interruptIndex = I2C3_EV_IRQn;
		break;

	default:
		break;
	}
	//1/42 = 24ns  5000ns/24ns = 208
	i2c.CCR = 208;
	//1/42 = 24ns  1000ns/24ns = 208
	i2c.TRISE = 42;
	i2c.CR1 = I2C_CR1_PE;
	//i2c.CR2 = I2C_CR2_ITEVTEN | I2C_CR2_ITBUFEN | I2C_CLK_SRC;
	i2c.CR2 = I2C_CLK_SRC;

//	i2c.BRR = UART_CLK_SRC / baudrate_;
//	// | i2c_CR1_TXEIE
//	i2c.CR1 = i2c_CR1_UE | i2c_CR1_RXNEIE | i2c_CR1_TE | i2c_CR1_RE;
	NVIC_EnableIRQ(interruptIndex);
	if(true)
	{
		I2cState = I2C_STATE_READY;
	}
	return result;
}
uint8_t I2c_io::Write(uint8_t *data, uint8_t size_)
{
	uint8_t result = 1;
	uint8_t dataIndex = 0;
	if (I2cState == I2C_STATE_READY)
	{
		I2cState = I2C_STATE_BUSY_TX;

		i2c.CR1 |= I2C_CR1_START;

		while (!(i2c.SR1 & I2C_SR1_SB))
		{
			asm("NOP");
		}
		i2c.SR1 &= ~I2C_SR1_ADDR;
		i2c.DR = (CurrentAddress << 1);

		while (!(i2c.SR1 & I2C_SR1_ADDR))
		{
			asm("NOP");
		}
		if (i2c.SR2 & I2C_SR2_PEC)
		{
			asm("NOP");
		}
		i2c.SR1 &= ~I2C_SR1_ADDR;
		while (!(i2c.SR1 & I2C_SR1_TXE))
		{
			asm("NOP");
		}

		while(dataIndex < size_)
		{
		i2c.DR = data[dataIndex];
		while (!(i2c.SR1 & I2C_SR1_TXE))
		{
			asm("NOP");
		}
		while ((i2c.SR1 & I2C_SR1_BTF))
		{
			asm("NOP");
		}
		dataIndex++;
		}
		i2c.CR1 |= I2C_CR1_STOP;
		while ((i2c.SR2 & I2C_SR2_BUSY))
		{
			asm("NOP");
		}
		I2cState = I2C_STATE_READY;
	}
	else
	{
		result = 0;
	}
	return result;
}
uint8_t I2c_io::Read(uint8_t *data_, uint8_t buffer_size_, uint8_t *size_re_)
{
	uint8_t result = 0;

	if (I2cState == I2C_STATE_READY)
	{
		I2cState = I2C_STATE_BUSY_TX;
		i2c.CR1 |= I2C_CR1_START;
		//read
		while (!(i2c.SR1 & I2C_SR1_SB))
		{
			asm("NOP");
		}
		i2c.SR1 &= ~I2C_SR1_ADDR;
		i2c.DR = (CurrentAddress << 1) | 1;

		while (!(i2c.SR1 & I2C_SR1_ADDR))
		{
			asm("NOP");
		}
		if (i2c.SR2 & I2C_SR2_PEC)

		{
			asm("NOP");
		}
		i2c.SR1 &= ~I2C_SR1_ADDR;

		while (!(i2c.SR1 & I2C_SR1_RXNE))
		{
			asm("NOP");
		}
		data_[0] = i2c.DR;
		*size_re_ = 1;
		I2cState = I2C_STATE_READY;
	}
	else
	{
		result = 0;
	}
	return result;
}



void I2c_io::Update() {

//	if (i2c.SR & i2c_FLAG_RXNE)
//	{
//		RxBuffer.in(i2c.DR);
//		i2c.CR1 |= i2c_CR1_TCIE;
//	}
//	if ((!TxBuffer.empty()))
//	{
//		if((i2c.SR & i2c_SR_TXE))
//		{
//		i2c.DR = TxBuffer.out();
//		}
//	}
//	else
//	{
//		i2c.CR1 &= ~i2c_CR1_TCIE;
//	}
}
