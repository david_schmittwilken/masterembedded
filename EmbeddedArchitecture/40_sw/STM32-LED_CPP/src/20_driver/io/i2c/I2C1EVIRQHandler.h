/*
 * I2C1EVIRQHandler.h
 *
 *  Created on: 06.12.2018
 *      Author: dave
 */

#ifndef I2C1EVIRQHANDLER_H_
#define I2C1EVIRQHANDLER_H_

#include "Handler.h"
#include "stm32f4xx.h"
#include "I2c_io.h"


class I2C1EVIRQHandler : public I2c_io
{
public:
	static I2C1EVIRQHandler *myThis;
	I2C1EVIRQHandler();
private:


};

#endif /* 20_DRIVER_IO_I2C1EVIRQHANDLER_H_ */
