/*
 * GPIO.cpp
 *
 *  Created on: Oct 24, 2018
 *      Author: david
 */
#include "GPIO.h"
#include "PORT.h"

void GPIO::setGPIOClocks()
{
	switch ((uint32_t)&gpio) {
	case (uint32_t)GPIOA_BASE:
		RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;
		break;
	case ((uint32_t)GPIOB_BASE):
		RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN;
		break;
	case ((uint32_t)GPIOC_BASE):
		RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN;
		break;
	case ((uint32_t)GPIOD_BASE):
		RCC->AHB1ENR |= RCC_AHB1ENR_GPIODEN;
		break;
	case ((uint32_t)GPIOE_BASE):
		RCC->AHB1ENR |= RCC_AHB1ENR_GPIOEEN;
		break;
	case ((uint32_t)GPIOF_BASE):
		RCC->AHB1ENR |= RCC_AHB1ENR_GPIOFEN;
		break;
	case ((uint32_t)GPIOG_BASE):
		RCC->AHB1ENR |= RCC_AHB1ENR_GPIOGEN;
		break;
	case ((uint32_t)GPIOH_BASE):
		RCC->AHB1ENR |= RCC_AHB1ENR_GPIOHEN;
		break;
	case ((uint32_t)GPIOI_BASE):
		RCC->AHB1ENR |= RCC_AHB1ENR_GPIOIEN;
		break;
	default:
		break;
	}
}

void GPIO::InitPin(uint8_t pinNr, PINCONFIGURATION pinConfig_){
	// Enable GPIO Peripheral clock
	GPIO::setGPIOClocks();
	/*reset all values for that pin and then set them*/
	/*reset*/
	RESETGPIOREGATTR(gpio.MODER, MODE_MAX, 2, pinNr);
	RESETGPIOREGATTR(gpio.OTYPER, OUTPUTTYPE_MAX, 1, pinNr);
	RESETGPIOREGATTR(gpio.OSPEEDR, OSPEED_MAX, 2, pinNr);
	RESETGPIOREGATTR(gpio.PUPDR, PULLUP_PULLDOWN_MAX, 2, pinNr);

	/*set*/
	if (pinConfig_ & ATTR_OUTPUT)
	{
		 SETGPIOREGATTR(gpio.MODER, OUTPUT, 2, pinNr);

		if (pinConfig_ & ATTR_OPENDRAIN)
		{
			 SETGPIOREGATTR(gpio.OTYPER, OPEN_DRAIN, 2, pinNr);
		}
	}
	/*TODO: make generic*/
	else if(pinConfig_ & ATTR_ALTERNATEFUNCTION)
	{
		SETGPIOREGATTR(gpio.MODER, ALTERNATE_FUNCTION, 2, pinNr);
	}

	 SETGPIOREGATTR(gpio.OSPEEDR, VERY_HIGH_SPEED, 2, pinNr);

	if (pinConfig_ & ATTR_PULLDOWN)
	{
		SETGPIOREGATTR(gpio.PUPDR, PULL_DOWN, 2, pinNr);
	} else if (pinConfig_ & ATTR_PULLUP)
	{
		SETGPIOREGATTR(gpio.PUPDR, PULL_UP, 2, pinNr);
	}
}

void GPIO::InitPin(uint8_t pinNr_, PINCONFIGURATION pinConfig_, PINFUNCTIONCONFIGURATION pinFunctionConfig_)
{
	GPIO::InitPin(pinNr_, pinConfig_);
	SETGPIOREGATTR(gpio.AFR[pinNr_/8], pinFunctionConfig_, 4, pinNr_ % 8);
}
	void GPIO::SetPin(uint8_t pinNr, uint8_t value, bool activeHigh)
	{
		/*TODO: add activehigh*/
		/*set value*/
		if(1 == value)
		{
				SETGPIOREGATTR(gpio.ODR, SET_VALUE, 1, pinNr);
		}
		else
		/*reset value*/
			RESETGPIOREGATTR(gpio.ODR,GPIOVALUE_MAX, 1, pinNr);
	}

	uint8_t GPIO::GetPin(uint8_t pinNr, bool activeHigh)
	{
		return ((gpio.IDR >> pinNr) & 1);
	}

	GPIO::GPIO(GPIO_TypeDef &gpio_):gpio(gpio_)
	{
		/*Nothing to do*/
	}

	void GPIO::TogglePin(uint8_t pinNr)
	{
		SetPin(pinNr, GetPin(pinNr, true) ^ 1, true);
	}
