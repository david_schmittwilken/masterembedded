/*
 * PIN.h
 *
 *  Created on: Oct 24, 2018
 *      Author: david
 */

#ifndef PIN_H_
#define PIN_H_

#include "stdio.h"
#include "PORT.h"

class PIN
{
public:
	PORT &port;
	uint8_t pinNr;

	void Set(uint8_t value_);
	uint8_t Get();
	void Toggle();

	PIN(PORT &port_, uint8_t pinNr);
};




#endif /* 20_DRIVER_PIN_H_ */
