/*
 * PIN.cpp
 *
 *  Created on: Oct 24, 2018
 *      Author: david
 */
#include "PIN.h"
#include "PORT.h"
#include "stdio.h"

	void PIN::Set(uint8_t value_)
	{
		PIN::port.SetPin(this->pinNr, value_, true);
	}
	uint8_t PIN::Get()
	{
		return PIN::port.GetPin(this->pinNr, true);
	}
	void PIN::Toggle()
	{
		PIN::port.TogglePin(this->pinNr);
	}

PIN::PIN(PORT &port_, uint8_t pinNr_):port(port_)
{
	pinNr = pinNr_;
}



