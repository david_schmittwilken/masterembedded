/*
 * PORT.h
 *
 *  Created on: Oct 24, 2018
 *      Author: david
 */

#ifndef PORT_H_
#define PORT_H_
#include "stm32f4xx.h"

class PORT
{
public:

	virtual void SetPin(uint8_t pinNr, uint8_t value, bool activeHigh) = 0;
	virtual uint8_t GetPin(uint8_t pinNr, bool activeHigh) = 0;
	virtual void TogglePin(uint8_t pinNr) = 0;

	virtual ~PORT() {}
};




#endif /* 20_DRIVER_PORT_H_ */
