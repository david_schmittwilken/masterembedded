/*
 * MotionSensor.h
 *
 *  Created on: 10.12.2018
 *      Author: dave
 */

#ifndef MOTIONSENSOR_H_
#define MOTIONSENSOR_H_
#include "I2c_io.h"


enum MOTION_SENSOR_REG
{
	MOTION_SENSOR_REG_STATUS_AUX 			= 0x07,
	MOTION_SENSOR_REG_OUT_TEMP_L 			= 0x0C,
	MOTION_SENSOR_REG_OUT_TEMP_H 			= 0x0D,
	MOTION_SENSOR_REG_WHO_AM_I 				= 0x0F,
	MOTION_SENSOR_REG_TEMP_CFG 				= 0x1F,
	MOTION_SENSOR_REG_CTRL_1 				= 0x20,
	MOTION_SENSOR_REG_CTRL_2 				= 0x21,
	MOTION_SENSOR_REG_CTRL_3 				= 0x22,
	MOTION_SENSOR_REG_CTRL_4 				= 0x23,
	MOTION_SENSOR_REG_CTRL_5 				= 0x24,
	MOTION_SENSOR_REG_CTRL_6 				= 0x25,
	MOTION_SENSOR_REG_DATACAPTURE 			= 0x26,
	MOTION_SENSOR_REG_STATUS 				= 0x27,
	MOTION_SENSOR_REG_OUT_X_L 				= 0x28,
	MOTION_SENSOR_REG_OUT_X_H 				= 0x29,
	MOTION_SENSOR_REG_OUT_Y_L 				= 0x2A,
	MOTION_SENSOR_REG_OUT_Y_H 				= 0x2B,
	MOTION_SENSOR_REG_OUT_Z_L 				= 0x2C,
	MOTION_SENSOR_REG_OUT_Z_H 				= 0x2D,
	MOTION_SENSOR_REG_FIFO_CTRL 			= 0x2E,
	MOTION_SENSOR_REG_FIFO_SRC 				= 0x2F,
	MOTION_SENSOR_REG_INT1_CFG 				= 0x30,
	MOTION_SENSOR_REG_INT1_SRC 				= 0x31,
	MOTION_SENSOR_REG_INT1_THS 				= 0x32,
	MOTION_SENSOR_REG_INT1_DURATION 		= 0x33,
	MOTION_SENSOR_REG_INT2_CFG 				= 0x34,
	MOTION_SENSOR_REG_INT2_SRC 				= 0x35,
	MOTION_SENSOR_REG_INT2_THS 				= 0x36,
	MOTION_SENSOR_REG_INT2_DURATION 		= 0x37,
	MOTION_SENSOR_REG_CLICK_CFG 			= 0x38,
	MOTION_SENSOR_REG_CLICK_SRC 			= 0x39,
	MOTION_SENSOR_REG_CLICK_THS 			= 0x3A,
	MOTION_SENSOR_REG_TIME_LIMIT 			= 0x3B,
	MOTION_SENSOR_REG_TIME_LATENCY 			= 0x3C,
	MOTION_SENSOR_REG_TIME_WINDOW 			= 0x3D,
	MOTION_SENSOR_REG_ACT_THS 				= 0x3E,
	MOTION_SENSOR_REG_ACT_DUR 				= 0x3F,
	MOTION_SENSOR_REG_INVALID				= 0xFF,
};

enum MOTION_SENSOR_CFG1
{
	MOTION_SENSOR_CFG1_XYZ_EN 				= ((0b111) 	<< (0)),
	MOTION_SENSOR_CFG1_LOWPOWER_EN 			= ((0b1) 	<< (3)),
	MOTION_SENSOR_CFG1_1_HZ 				= ((0b0001) << (4)),
	MOTION_SENSOR_CFG1_10_HZ 				= ((0b0010) << (4)),
	MOTION_SENSOR_CFG1_25_HZ 				= ((0b0011) << (4)),
	MOTION_SENSOR_CFG1_50_HZ 				= ((0b0100) << (4)),
	MOTION_SENSOR_CFG1_100_HZ 				= ((0b0101) << (4)),
	MOTION_SENSOR_CFG1_200_HZ 				= ((0b0110) << (4)),
	MOTION_SENSOR_CFG1_400_HZ 				= ((0b0111) << (4)),
	MOTION_SENSOR_CFG1_LOW_POWER_1620_HZ 	= ((0b1000) << (4)),
	MOTION_SENSOR_CFG1_HR_NORM_1344__HZ 	= ((0b1001) << (4)),
	MOTION_SENSOR_CFG1_LOW_POWER_5376_HZ 	= ((0b1001) << (4)),

};

enum MOTION_SENSOR_CFG4
{
	MOTION_SENSOR_CFG4_HIGH_RES_EN 			= ((0b1) 	<< (3)),
	MOTION_SENSOR_CFG4_SCALE_2G_EN 			= ((0b00) 	<< (4)),
	MOTION_SENSOR_CFG4_SCALE_4G_EN 			= ((0b01) 	<< (4)),
	MOTION_SENSOR_CFG4_SCALE_8G_EN 			= ((0b10) 	<< (4)),
	MOTION_SENSOR_CFG4_SCALE_16G_EN 		= ((0b11) 	<< (4)),
	MOTION_SENSOR_CFG4_BLOCK_DATA_EN 		= ((0b1) 	<< (7)),
};

enum MOTION_SENSOR_TEMP_CFG
{
	MOTION_SENSOR_TEMP_CFG_TEMP_EN 			= ((0b11) 	<< (6)),
};

enum AXIS
{
	AXIS_X,
	AXIS_Y,
	AXIS_Z,
};

enum OPERATING_MODE
{
	OPERATING_MODE_LOW_POWER,
	OPERATING_MODE_NORMAL,
	OPERATING_MODE_HIGH_RES,
};

union MOTION_TEMP_STATUS_REG
{
	uint8_t buf;
	struct
	{
		unsigned int reserved1 			:2;
		unsigned int temp_dataAvailable :1;
		unsigned int reserved2 			:3;
		unsigned int temp_overRun 		:1;
		unsigned int reserved3 			:1;
	}data;
};

union MOTION_STATUS_REG
{
	uint8_t buf;
	struct
	{
		unsigned int ZYX_overRun 		:1;
		unsigned int Z_overRun 			:1;
		unsigned int Y_overRun 			:1;
		unsigned int X_overRun 			:1;
		unsigned int ZYX_dataAvailable 	:1;
		unsigned int Z_dataAvailable 	:1;
		unsigned int Y_dataAvailable 	:1;
		unsigned int X_dataAvailable 	:1;
	}data;
};


class MotionSensor
{
public:
	/*sensor register values*/
	MOTION_TEMP_STATUS_REG TemperatureStatus = {0};
	MOTION_STATUS_REG Status = {0};
	int8_t TemperatureValue = 0;
	uint8_t WhoAmI = 0;

	int16_t GetAxisAcceleration(AXIS axis);
	void ReadTemperature();
	void ReadTemperatureStatus();
	void ReadStatus();
	MotionSensor(I2c_io &i2C_io_, uint8_t address_);
	~MotionSensor(){};

private:
	I2c_io &I2C_io;
	uint8_t &address;

	OPERATING_MODE operatingMode;
	int8_t tempOffset = 20;
	void Init();
	uint8_t ReadRegister(MOTION_SENSOR_REG reg_);
	void WriteRegister(MOTION_SENSOR_REG reg_, uint8_t value);

};

#endif /* 20_DRIVER_SENSORS_ACTUATORS_MOTIONSENSOR_H_ */
