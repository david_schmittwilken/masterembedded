/*
 * MotionSensor.cpp
 *
 *  Created on: 10.12.2018
 *      Author: dave
 */

#include "MotionSensor.h"

void MotionSensor::Init()
{
	/*default sensor initialization*/
	uint8_t reg_ctrl1 = (MOTION_SENSOR_CFG1_100_HZ | MOTION_SENSOR_CFG1_XYZ_EN);
	uint8_t reg_ctrl4 = (MOTION_SENSOR_CFG4_SCALE_2G_EN | MOTION_SENSOR_CFG4_BLOCK_DATA_EN);
	switch(operatingMode)
	{
	case OPERATING_MODE_LOW_POWER: reg_ctrl1 |= MOTION_SENSOR_CFG1_LOWPOWER_EN;
								break;
	case OPERATING_MODE_NORMAL: /*Nothing to do*/
								break;
	case OPERATING_MODE_HIGH_RES: reg_ctrl4 |= MOTION_SENSOR_CFG4_HIGH_RES_EN;
								break;
	default: break;
	}

	/*configure sensor*/
	WriteRegister(MOTION_SENSOR_REG_TEMP_CFG, MOTION_SENSOR_TEMP_CFG_TEMP_EN);
	WriteRegister(MOTION_SENSOR_REG_CTRL_1, reg_ctrl1);
	WriteRegister(MOTION_SENSOR_REG_CTRL_4, reg_ctrl4);
}

MotionSensor::MotionSensor(I2c_io &i2C_io_, uint8_t address_):I2C_io(i2C_io_), address(address_)
{
	operatingMode = OPERATING_MODE_LOW_POWER;
	I2C_io.Init(0);
	Init();
}

int16_t MotionSensor::GetAxisAcceleration(AXIS axis)
{
	int16_t value;
	uint8_t regLowValue;
	uint8_t regHighValue;

	MOTION_SENSOR_REG regLow = MOTION_SENSOR_REG_INVALID;
	MOTION_SENSOR_REG regHigh = MOTION_SENSOR_REG_INVALID;


	value = 0;
	regLowValue = 0;
	regHighValue = 0;

	switch (axis)
	{
	case AXIS_X:
		regLow = MOTION_SENSOR_REG_OUT_X_L;
		regHigh = MOTION_SENSOR_REG_OUT_X_H;
		break;
	case AXIS_Y:
		regLow = MOTION_SENSOR_REG_OUT_Y_L;
		regHigh = MOTION_SENSOR_REG_OUT_Y_H;
		break;
	case AXIS_Z:
		regLow = MOTION_SENSOR_REG_OUT_Z_L;
		regHigh = MOTION_SENSOR_REG_OUT_Z_H;
		break;
	default:
		break;
	}

	regLowValue = ReadRegister(regLow);
	regHighValue = ReadRegister(regHigh);

	value =  (int16_t)((int8_t)regLowValue + (int8_t((int8_t)(regHighValue)) *256));

	value = (int32_t)value*1000/(1024*16);

	return value;
}

void MotionSensor::ReadTemperature()
{
	/*both registers have to be read according to datasheet*/
	(void)ReadRegister(MOTION_SENSOR_REG_OUT_TEMP_L);
	this->TemperatureValue = ((int8_t)ReadRegister(MOTION_SENSOR_REG_OUT_TEMP_H)) + tempOffset;
}

void MotionSensor::ReadTemperatureStatus()
{
	this->TemperatureStatus.buf = (uint8_t) (ReadRegister(MOTION_SENSOR_REG_STATUS_AUX));
}

void MotionSensor::ReadStatus()
{
	this->Status.buf = (uint8_t) (ReadRegister(MOTION_SENSOR_REG_STATUS));
}

uint8_t MotionSensor::ReadRegister(MOTION_SENSOR_REG reg_)
{
uint8_t value = 0;
uint8_t bufsize_rcv;
I2C_io.CurrentRegister = reg_;
I2C_io.Write((uint8_t *)&reg_, 1);
I2C_io.Read((uint8_t *)&value, 1, &bufsize_rcv);

return value;
}

void MotionSensor::WriteRegister(MOTION_SENSOR_REG reg_, uint8_t value)
{
	uint8_t buf[2];
	buf[0] = reg_;
	buf[1] = value;
	I2C_io.Write(&buf[0], 2);
}
