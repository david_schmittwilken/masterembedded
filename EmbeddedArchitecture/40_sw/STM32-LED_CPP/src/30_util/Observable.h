/*
 * Observable.h
 *
 *  Created on: 02.12.2018
 *      Author: dave
 */

#ifndef OBSERVABLE_H_
#define OBSERVABLE_H_

#include <cstdio>
#include <cstdlib>

#include "Handler.h"

#define HANDLERLIST_COUNT 10

class Observable
{
public:
	Handler *handler[HANDLERLIST_COUNT];
	uint8_t currentHandlerIndex = 0;

	void AddHandler(Handler *handler_)
	{
		if (currentHandlerIndex < HANDLERLIST_COUNT)
		{
			handler[currentHandlerIndex] = handler_;
			currentHandlerIndex++;
		}
	};

	virtual void Update() = 0;
	Observable(){
		for(uint8_t i = 0; i < HANDLERLIST_COUNT; i++)
		{
			handler[i] = NULL;
		}
	};
	virtual ~Observable(){};
protected:
	IRQn_Type interruptIndex = INVALID_IRQn;
};

#endif /* 30_UTIL_OBSERVABLE_H_ */
