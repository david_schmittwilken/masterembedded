/*
 * FermatFactorization.h
 *
 *  Created on: 11.11.2018
 *      Author: dave
 */

#ifndef FERMATFACTORIZATION_H_
#define FERMATFACTORIZATION_H_
#include "stdint.h"

class FermatFactorization
{
public:
static void FermatFactor(uint32_t factor, uint32_t &factor1, uint16_t &factor2);

private:
static bool isSquare(uint32_t factor);
};



#endif /* 20_DRIVER_FERMATFACTORIZATION_H_ */
