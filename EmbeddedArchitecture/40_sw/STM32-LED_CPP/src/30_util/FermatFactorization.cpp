/*
 * FermatFactorization.cpp
 *
 *  Created on: 11.11.2018
 *      Author: dave
 */
#include "FermatFactorization.h"
#include "cmath"

void FermatFactorization::FermatFactor(uint32_t factor, uint32_t &factor1,
		uint16_t &factor2)
{
	uint16_t TimesMultByTwo = 0;

	/*divide factor by 2 until we have an odd value because fermat only works with odd numbers*/
	while (factor % 2 == 0)
	{
		factor /= 2;
		TimesMultByTwo++;
	}
	uint32_t a = (uint32_t) ceil(sqrt(factor));
	uint32_t b2 = a * a - factor;
	while (!isSquare(b2))
	{
		a++;
		b2 = a * a - factor;
	}
	factor1 = a - (uint32_t) sqrt(b2);
	factor2 = factor / factor1;
	while (TimesMultByTwo > 0)
	{
		if (factor1 < factor2)
		{
			factor1 *= 2;
		}
		else
		{
			factor2 *= 2;
		}
		TimesMultByTwo--;
	}
}

    bool FermatFactorization::isSquare(uint32_t factor)
    {
    	uint32_t sqr = (uint32_t) sqrt(factor);
        if (sqr * sqr == factor || (sqr + 1) * (sqr + 1) == factor)
            return true;
        return false;
    }
