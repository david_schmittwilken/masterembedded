/*
 * CircularBuffer.h
 *
 *  Created on: 02.12.2018
 *      Author: dave
 */

#ifndef CIRCULARBUFFER_H_
#define CIRCULARBUFFER_H_

#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <algorithm>

template<typename T, const std::size_t N>
class CircularBuffer
{
public:
	typedef T value_type;
	typedef value_type* pointer;
	typedef const value_type* const_pointer;
	typedef std::size_t size_type;
	typedef value_type& reference;
	typedef const value_type& const_reference;

	CircularBuffer(const T& value = value_type(), const size_type count =
			size_type(0U)) :
			in_ptr(buffer), out_ptr(buffer)
	{
		const size_type the_count = (std::min)(N, count);
		std::fill(in_ptr, in_ptr + the_count, value);
		in_ptr += the_count;
	}
	CircularBuffer(const CircularBuffer& other) :
			in_ptr(other.in_ptr), out_ptr(other.out_ptr)
	{
		std::copy(other.buffer, other.buffer + N, buffer);
	}
	CircularBuffer& operator=(const CircularBuffer& other)
	{
		if (this != &other)
		{
			in_ptr(other.in_ptr);
			out_ptr(other.out_ptr);
			std::copy(other.buffer, other.buffer + N, buffer);
		}
		return *this;
	}
	size_type capacity() const
	{
		return N;
	}
	bool empty() const
	{
		return (in_ptr == out_ptr);
	}
	size_type size() const
	{
		const bool is_wrap = (in_ptr < out_ptr);
		return size_type(
				(is_wrap == false) ?
						size_type(in_ptr - out_ptr) :
						N - size_type(out_ptr - in_ptr));
	}
	void clear()
	{
		in_ptr = buffer;
		out_ptr = buffer;
	}
	void in(const value_type value)
	{
		if (in_ptr >= (buffer + N))
		{
			in_ptr = buffer;
		}
		*in_ptr = value;
		++in_ptr;
	}
	value_type out()
	{
		if (out_ptr >= (buffer + N))
		{
			out_ptr = buffer;
		}
		const value_type value = *out_ptr;
		++out_ptr;
		return value;
	}
	reference front()
	{
		return ((out_ptr >= (buffer + N)) ? buffer[N - 1U] : *out_ptr);
	}
	const_reference front() const
	{
		return ((out_ptr >= (buffer + N)) ? buffer[N - 1U] : *out_ptr);
	}
	reference back()
	{
		return ((in_ptr >= (buffer + N)) ? buffer[N - 1U] : *in_ptr);
	}
	const_reference back() const
	{
		return ((in_ptr >= (buffer + N)) ? buffer[N - 1U] : *in_ptr);
	}
private:
	value_type buffer[N];
	pointer in_ptr;
	pointer out_ptr;
};


#endif /* 30_UTIL_CIRCULARBUFFER_H_ */
