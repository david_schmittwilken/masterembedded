/*
 * Handler.h
 *
 *  Created on: 07.11.2018
 *      Author: Student
 */

#ifndef HANDLER_H_
#define HANDLER_H_

class Handler
{
public:
	virtual void Update(void) = 0;
	virtual ~Handler() {}
};

#endif /* 20_DRIVER_HANDLER_H_ */
