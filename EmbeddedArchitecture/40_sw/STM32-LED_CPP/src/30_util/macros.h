/*
 * macros.h
 *
 *  Created on: 02.11.2018
 *      Author: dave
 */

#ifndef MACROS_H_
#define MACROS_H_

#define RESETGPIOREGATTR(REG, MAXVAL, FACTOR, PIN) REG&=~((MAXVAL) << ((FACTOR)*(PIN)))
#define SETGPIOREGATTR(REG, VAL, FACTOR, PIN) REG|=((VAL) << ((FACTOR)*(PIN)))

/* Bit setzen */
#define SETBITS(var, bits, bitbase) ((var) |= (bits << bitbase))

/* Bit l�schen */
#define RESETBITS(var, bits, bitbase) ((var) &= (unsigned)~(bits << bitbase))

/* Bit togglen */
#define TOGGLEBITS(var,bits) ((var) ^= (bits))

/* Bit abfragen */
#define BITSSET(var, bits) ((var) & (bits))
#define BITSCLEARED(var, bits) !BITSSET(var, bit)

#endif /* MACROS_H_ */
